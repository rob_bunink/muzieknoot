﻿using System;
using WinFormMVC.Controller;
using WinFormMVC.View;

namespace UseMVCApplication
{
    static class Program
    {
        private static MainView view;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            view = new MainView();
            view.Visible = false;

            // Add some dummy data
          

            MainViewController controller = new MainViewController(view);
            controller.LoadView();
            view.ShowDialog();
        }
    }
}
