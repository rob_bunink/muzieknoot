﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MuziekNootApplication.View;

namespace MuziekNootApplication.Controller
{
    interface ISubject
    {
         
        /// <summary>
        /// Registers the observer.
        /// </summary>
        /// <param name="o">The observer.</param>
        void RegisterObserver(IObserver o);


        /// <summary>
        /// Removes the observer.
        /// </summary>
        /// <param name="o">The observer.</param>
        void RemoveObserver(IObserver o);


        /// <summary>
        /// Notifies the observers.
        /// </summary>
        void NotifyObservers();
    }
}
