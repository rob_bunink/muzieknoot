﻿//Copyright (C) MuziekNoot. All right reserved.
//MainViewController.cs
//compile with: /doc: MainViewController.xml
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MuziekNootApplication.Model;
using MuziekNootApplication.Model.MediaPlayer;
using MuziekNootApplication.View;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Timers;

namespace MuziekNootApplication.Controller
{

    /// <summary>
    /// MainViewController
    /// </summary>
    public class MainViewController : ISubject
    {
        /// <summary>
        /// the audio timer
        /// </summary>
        private System.Timers.Timer aTimer = new System.Timers.Timer();

        /// <summary>
        /// The audioplayer
        /// </summary>
        private readonly AudioPlayer _audioplayer = new AudioPlayer();

        /// <summary>
        /// The observers
        /// </summary>
        protected List<IObserver> observers;

        /// <summary>
        /// The start ticks when play is pressed
        /// </summary>
        private int startTicks;

        /// <summary>
        /// The start position of the loop
        /// </summary>
        private ulong startStopPosition;

        /// <summary>
        /// The stop position of the loop
        /// </summary>
        private ulong stopStopPosition;

        /// <summary>
        /// bool to check if the loop is enabled
        /// </summary>
        private bool stopperEnabled ;

        /// <summary>
        /// The instance
        /// </summary>
        private static readonly MainViewController instance = new MainViewController(_view);

        /// <summary>
        /// The tick count
        /// </summary>
        private int TickCount = 0;

        /// <summary>
        /// The filename
        /// </summary>
        private string filename;

        /// <summary>
        /// The timer identifier
        /// </summary>
        private int mTimerId;

        /// <summary>
        /// The timer event handler
        /// </summary>
        private TimerEventHandler _mHandler;  // NOTE: declare at class scope so garbage collector doesn't release it!!!
        private int _mTestTick;
        private DateTime _mTestStart;

        // P/Invoke declarations
        private delegate void TimerEventHandler(int id, int msg, IntPtr user, int dw1, int dw2);
        private const int TIME_PERIODIC = 1;
        private const int EVENT_TYPE = TIME_PERIODIC;// + 0x100;  // TIME_KILL_SYNCHRONOUS causes a hang ?!

        /// <summary>
        /// called to start a timer
        /// </summary>
        /// <param name="delay">The delay.</param>
        /// <param name="resolution">The resolution.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="user">The user.</param>
        /// <param name="eventType">Type of the event.</param>
        /// <returns></returns>
        [DllImport("winmm.dll")]
        private static extern int timeSetEvent(int delay, int resolution, TimerEventHandler handler, IntPtr user, int eventType);

        /// <summary>
        /// callewd to kill a timer
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [DllImport("winmm.dll")]
        private static extern int timeKillEvent(int id);
        [DllImport("winmm.dll")]
        private static extern int timeBeginPeriod(int msec);
        [DllImport("winmm.dll")]
        private static extern int timeEndPeriod(int msec);

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewController"/> class.
        /// </summary>
        /// <param name="view">The view.</param>
        private MainViewController(IView view)
        {
            observers = new List<IObserver>();
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public static MainViewController Instance
        {
            get
            {
                return instance;
            }
        }


        /// <summary>
        /// The _view
        /// </summary>
        public static IView _view;


        /// <summary>
        /// Registers the observer.
        /// </summary>
        /// <param name="o">The observer.</param>
        public void RegisterObserver(IObserver o)
        {
            observers.Add(o);
        }

        /// <summary>
        /// Removes the observer.
        /// </summary>
        /// <param name="o">The observer.</param>
        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }

        /// <summary>
        /// Notifies the observers.
        /// </summary>
        public void NotifyObservers()
        {
            foreach (IObserver o in observers)
                o.RecieveUpdate(TickCount);
        }

        /// <summary>
        /// Nummer inladen
        /// </summary>
        /// <param name="filename"></param>
        public void AddSong(string filename)
        {
            _view.GetCurrentSongLabel().Text = filename;
            //Open bestand in een player
            _audioplayer.OpenFile("mp3", filename);
            ulong length = _audioplayer.GetLength();
            TimeSpan time = TimeSpan.FromMilliseconds(length);
            _view.GetTbPostion().Maximum = (int)length / 1000;
            _view.GetLengthLabel().Text = (time.ToString(@"hh\:mm\:ss"));
            _view.GetCurrentSongLabel().Text = _audioplayer.GetInfo();
            PositionUpdate();
        }



        /// <summary>
        /// Adds a empty note.
        /// </summary>
        public void AddNote()
        {
            var a = new GuitarChord();
            a.Timestamp = (decimal) (TickCount*0.01);
            AddNotes(a);
        }

        /// <summary>
        /// Adds a note.
        /// </summary>
        /// <param name="note">The note.</param>
        public void AddNotes(INote note)
        {
            var a = _view.GetTimeline();
            a.AddNote(note);
        }

        /// <summary>
        /// Plays the song and starts a timer.
        /// </summary>
        public void PlaySong()
        {
            var a = _view.GetTimeline();
            this.RegisterObserver(a);
            aTimer.Enabled = true;
            _view.getPositionTimer().Enabled = true;
            _audioplayer.Play();
            _view.RestoreTimeline();
            if (filename != String.Empty)
            {
                this._mHandler = new TimerEventHandler(TimerEventProcessor);
                mTimerId = timeSetEvent(10, 0,_mHandler, IntPtr.Zero, 1);
                // loop = new System.Threading.Timer(TimerEventProcessor,null,0,50);
            }
        }

        /// <summary>
        /// method called when a tick has passed
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="user">The user.</param>
        /// <param name="dw1">The DW1.</param>
        /// <param name="dw2">The DW2.</param>
        private void TimerEventProcessor(int id, int msg, IntPtr user, int dw1, int dw2)
        {
            this.NotifyObservers();
            TickCount += 1;
        }

        /// <summary>
        /// Stops the song.
        /// </summary>
        public void StopSong()
        {
            timeKillEvent(mTimerId);
          
                _view.getPositionTimer().Enabled = false;
                aTimer.Enabled = false;
            
            _audioplayer.Stop();
            TickCount = 0;
        }

        /// <summary>
        /// Pauzes the song.
        /// </summary>
         public void PauzeSong()
        {
            if (_audioplayer.IsPlaying())
            {
                _view.getPositionTimer().Enabled = false;
                aTimer.Enabled = false;
                _audioplayer.Pauze();
            }
            timeKillEvent(mTimerId);
        }

    

      

        /// <summary>
        /// Zet het geluid uit of aan.
        /// </summary>
        /// <param name="checked"></param>
        public void SetMuteAll(bool @checked)
        {
            _audioplayer.SetMuteAll(@checked);
        }

        /// <summary>
        /// Zet de loop functie aan.
        /// </summary>
        /// <param name="checked"></param>
        public void SetLoop(bool @checked)
        {
            _audioplayer.SetLoop(@checked);
        }


        /// <summary>
        /// Zet het volume op een andere waarde.
        /// </summary>
        /// <param name="value"></param>
        public void SetVolume(int value)
        {
            _audioplayer.SetVolume(value);
        }

        /// <summary>
        /// Zet de belance op een andere waarde.
        /// </summary>
        /// <param name="value"></param>
        public void SetBalance(int value)
        {
            _audioplayer.SetBalance(value);
        }

        /// <summary>
        /// Zet de bass op een andere waarde.
        /// </summary>
        /// <param name="value"></param>
        public void SetBass(int value)
        {
            _audioplayer.SetBass(value);
        }

        /// <summary>
        /// Zet de treble op een andere waarde.
        /// </summary>
        /// <param name="value"></param>
        public void SetTreble(int value)
        {
            _audioplayer.SetTreble(value);
        }
        /// <summary>
        /// Bepaal de positie in het lied.
        /// </summary>
        /// <param name="ulong"></param>
        public void SetPosition(ulong @ulong)
        {
            PauzeSong();
            _audioplayer.SetPosition(@ulong);
            PositionUpdate();
            TickCount = startTicks;
            PlaySong();
        }

        /// <summary>
        /// Reset stopper
        /// </summary>
        public void ResetStopper()
        {
            startStopPosition = 0;
            stopStopPosition = 0;
            startTicks = 0;
            stopperEnabled = false;
            _view.GetlbStartStopper().Text = "";
            _view.GetlbStopStopper().Text = "";
            _view.GetResetStopperButton().Enabled = false;
            _view.GetTbPostion().Enabled = true;
        }

        /// <summary>
        /// Stopper functie wordt aangeroepen
        /// </summary>
        public void Stopper()
        {
            TimeSpan time;
            if (startStopPosition > 0 && stopStopPosition > 0 && stopperEnabled)
            {
                SetPosition(startStopPosition);
            }
            else if (startStopPosition == 0)
            {
                startTicks = TickCount;
                startStopPosition = _audioplayer.GetCurrentPosition();
                 time = TimeSpan.FromMilliseconds(startStopPosition);
                _view.GetlbStartStopper().Text = (time.ToString(@"hh\:mm\:ss"));
                _view.GetResetStopperButton().Enabled = true;
            }
            else if (stopStopPosition == 0)
            {
                stopStopPosition = _audioplayer.GetCurrentPosition();
                time = TimeSpan.FromMilliseconds(stopStopPosition);
                _view.GetlbStopStopper().Text = (time.ToString(@"hh\:mm\:ss"));
            }
            else
            {
                stopperEnabled = true;
                _view.GetTbPostion().Enabled = false;
            }
        }

        /// <summary>
        /// Update de huidige positie teller
        /// </summary>
        public void PositionUpdate()
        {
            ulong position;
            if (_audioplayer.IsPlaying())
            {
                position = _audioplayer.GetCurrentPosition();
                //Positie is gelijk aan de stop/start posities
                if ((stopperEnabled) &&(position > stopStopPosition))
                {
                    PauzeSong();
                }
            }
            else
            {
                bool result = ulong.TryParse(_view.GetPositionLabel().Text, out position);
                if (result)
                {
                    position = position*1000;
                }
                else
                {
                    position = 0;
                }
            }
            TimeSpan time = TimeSpan.FromMilliseconds(position);
            _view.GetPositionLabel().Text = (time.ToString(@"hh\:mm\:ss"));
            _view.GetTbPostion().Value = (int)position / 1000;

        }

    }
}
