﻿//Copyright (C) MuziekNoot. All right reserved.
//ITimeline.cs
//compile with: /doc: ITimeline.xml

using System.Collections.Generic;
using System.Windows.Forms;
using MuziekNootApplication.Model;

namespace MuziekNootApplication.View
{
    public interface ITimeline : IObserver
    {
      
        void Paint(PaintEventArgs e);
      

        void AddNote(INote note);
    }
}