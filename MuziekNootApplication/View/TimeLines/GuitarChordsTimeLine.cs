﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MuziekNootApplication.Model;
using System.Reflection;

namespace MuziekNootApplication.View
{

    public partial class GuitarChordsTimeLine : Control, ITimeline, IObserver 
    {
        /// <summary>
        /// The notes
        /// </summary>
        public static List<GuitarChord> Notes;

    
        /// <summary>
        /// The tick count
        /// </summary>
        public int TickCount = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="GuitarChordsTimeLine"/> class.
        /// </summary>
        public GuitarChordsTimeLine()
        {
            this.SetStyle(
                ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.ContainerControl |
                ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);
        
            InitializeComponent();

            Notes = new List<GuitarChord>();
          
            this.Width = 999999999;
        }

        /// <summary>
        /// Recieves the update from ther observer method.
        /// </summary>
        /// <param name="TickCount">The tick count.</param>
        public void RecieveUpdate(int TickCount)
        {
            this.TickCount = TickCount;
            this.Invalidate();
        }

        /// <summary>
        /// Paints evernt handler to set the width of the timeline
        /// </summary>
        /// <param name="e">The <see cref="PaintEventArgs"/> instance containing the event data.</param>
        public void Paint(PaintEventArgs e)
        {
            this.Width = Notes.Count * 550;
        }

        /// <summary>
        /// Adds a note.
        /// </summary>
        /// <param name="note">The note.</param>
        public void AddNote(INote note)
        {
            Notes.Add((GuitarChord) note);
        }

        /// <summary>
        /// Paint the standard empty note container, then for each note draws a line or a note container, depending on the previous note.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.DrawLine(new Pen(Color.Blue), 50, 0, 50, 30);
            e.Graphics.DrawEllipse(new Pen(Color.Blue), 29, 30, 41, 41);
            e.Graphics.DrawLine(new Pen(Color.Blue), 50, 71, 50, 120);
            var a = 0;
            foreach (var note in Notes)
            {
                var i = Convert.ToInt32(note.Timestamp * 100 + 32);
                if (i - TickCount > 31)
                {
                    if (a != 0)
                    {
                        if (note.DisplayName == Notes[a - 1].DisplayName)
                        {
                            e.Graphics.DrawLine(new Pen(Color.Blue), i + 18 - TickCount*1, 0, i + 18 - TickCount*1, 100);
                        }
                        else
                        {
                            e.Graphics.DrawLine(new Pen(Color.Blue), i + 18 - TickCount * 1, 0, i + 18 - TickCount * 1, 30);
                            e.Graphics.DrawEllipse(new Pen(Color.Blue), i - 3 - TickCount * 1, 30, 41, 41);
                            e.Graphics.DrawLine(new Pen(Color.Blue), i + 18 - TickCount * 1, 71, i + 18 - TickCount * 1, 100);
                            e.Graphics.DrawString(note.DisplayName, new Font(FontFamily.GenericSansSerif, 15),
                                new SolidBrush(Color.Black), i - TickCount * 1, 38);
                        }
                    }
                    else
                    {
                        e.Graphics.DrawLine(new Pen(Color.Blue), i + 18 - TickCount*1, 0, i + 18 - TickCount*1, 30);
                        e.Graphics.DrawEllipse(new Pen(Color.Blue), i - 3 - TickCount*1, 30, 41, 41);
                        e.Graphics.DrawLine(new Pen(Color.Blue), i + 18 - TickCount*1, 71, i + 18 - TickCount*1, 100);
                        e.Graphics.DrawString(note.DisplayName, new Font(FontFamily.GenericSansSerif, 15),
                            new SolidBrush(Color.Black), i - TickCount*1, 38);
                    }
                }
                a++;
            }
        }

     
    }

}