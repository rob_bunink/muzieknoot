﻿using System.Drawing;
using System.Windows.Forms;

namespace MuziekNootApplication.View
{
    partial class GuitarChordsTimeLine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // GuitarChordsTimeLine
            // 
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint  | ControlStyles.OptimizedDoubleBuffer, true);
            this.DoubleBuffered = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Name = "GuitarChordsTimeLine";
            this.Size = new System.Drawing.Size(0, 98);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
