﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MuziekNootApplication.Controller;
using MuziekNootApplication.Model.MediaPlayer;
using MuziekNootApplication.View.AddNotes;

namespace MuziekNootApplication.View
{
    public partial class MainView : Form, IView
    {
        #region Singleton Pattern
        private static readonly MainView instance = new MainView();
        private Control TimeLine;

        private MainView()
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.UserPaint |
                 ControlStyles.AllPaintingInWmPaint |
                 ControlStyles.ResizeRedraw |
                 ControlStyles.ContainerControl |
                 ControlStyles.OptimizedDoubleBuffer |
                 ControlStyles.SupportsTransparentBackColor
                 , true);
            InitializeComponent();
            this.TimeLineContainer.Controls.Add(new GuitarChordsTimeLine());
            TimeLine = (Control)this.TimeLineContainer.Controls.OfType<ITimeline>().First();
        }

        public static MainView Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion 

        MainViewController _controller;

        #region Events raised  back to controller

        private void AddNotes(object sender, EventArgs e)
        {
            //_controller.AddNotes();
            this.Controls.Add(new AddGuitarChords());
            this.Controls.OfType<AddGuitarChords>().First().BringToFront();
        }

        private void StopSong(object sender, EventArgs e)
        {
            _controller.StopSong();
            PauzeSongButton.Enabled = false;
            PlaySongButton.Enabled = true;
        }

        /// <summary>
        /// Nummer afspelen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlaySong(object sender, EventArgs e)
        {
            _controller.PlaySong();
            PlaySongButton.Enabled = false;
            PauzeSongButton.Enabled = true;
        }

        /// <summary>
        /// Nummer inladen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadSong(object sender, EventArgs e)
        {
            using(OpenFileDialog dlgOpen = new OpenFileDialog())
            {
                dlgOpen.Filter = "Mp3 File|*.mp3";
                dlgOpen.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
                if (dlgOpen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    _controller.AddSong(dlgOpen.FileName);
                    PlaySongButton.Enabled = true;
                    PauzeSongButton.Enabled = false;
                }
            }
        }


        public ITimeline GetTimeline()
        {
            return this.TimeLineContainer.Controls.OfType<ITimeline>().First();
            

        }



        public void RestoreTimeline()
        {
            TimeLine.Location = new System.Drawing.Point(0, 0);
        }

        public TrackBar GetTbPostion()
        {
            return this.tbPosition;
        }

        public Label GetlbStartStopper()
        {
            return this.lbStartStopper;
        }


        public Label GetlbStopStopper()
        {
            return this.lbStopStopper;
        }

        public Button GetResetStopperButton()
        {
            return ResetStopperButton;
        }


        public void MoveTimeLine(int px)
        {
          //var timeLine=  (Control) this.TimeLineContainer.Controls.OfType<ITimeline>().First();
            TimeLine.Location = new System.Drawing.Point(TimeLine.Left - px, TimeLine.Top);
        }

        public Label GetCurrentSongLabel()
        {
            return this.lblCurrentSong;
        }

        public Label GetLengthLabel()
        {
            return this.lblLength;
        }

        public Label GetPositionLabel()
        {
            return this.lblPositie;
        }

        #endregion

        #region ICatalogView implementation

        /// <summary>
        /// Positie timer ophalen
        /// </summary>
        /// <returns></returns>
        public Timer getPositionTimer()
        {
            return positionTimer;
        }

        public void SetController(MainViewController controller)
        {
            _controller = controller;
        }


        #endregion

        /// <summary>
        /// Liedje wordt op pauze gezet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PauzeSong(object sender, EventArgs e)
        {
            _controller.PauzeSong();
            PauzeSongButton.Enabled = false;
            PlaySongButton.Enabled = true;
        }


        /// <summary>
        /// Mute wordt aangezet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbMute_CheckedChanged(object sender, EventArgs e)
        {
            _controller.SetMuteAll(cbMute.Checked);
        }



        /// <summary>
        /// De loop checked box wordt aan of uitgezet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbLoop_CheckedChanged_1(object sender, EventArgs e)
        {
            _controller.SetLoop(cbLoop.Checked);
        }

        /// <summary>
        /// Volume veranderd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbVolume_ValueChanged(object sender, EventArgs e)
        {
            _controller.SetVolume(tbVolume.Value);
        }

        /// <summary>
        /// Balance wordt aangepast
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbBalance_ValueChanged(object sender, EventArgs e)
        {
            _controller.SetBalance(tbBalance.Value);
        }

        /// <summary>
        /// De bass wordt aangepast.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbBass_ValueChanged(object sender, EventArgs e)
        {
            _controller.SetBass(tbBass.Value);
        }

        /// <summary>
        /// De treble wordt aangepast.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbTreble_ValueChanged(object sender, EventArgs e)
        {
            _controller.SetTreble(tbBass.Value);
        }

        /// <summary>
        /// De positie wordt aangepast
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbPosition_MouseUp(object sender, MouseEventArgs e)
        {
            _controller.SetPosition((ulong)tbPosition.Value*1000);
        }


        /// <summary>
        /// De override methode ProcessCmdKey wordt gebruikt om de toetsenboard input van de pijltjes toetsen te ontvangen en als input device te gebruiken voor de game.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData">De toets die gedrukt is</param>
        /// <returns>Stuur de toetseninput terug naar scherm, zodat de standaard toetsen wel blijven worden.</returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData.Equals(Keys.Space))
            {
                _controller.Stopper();

                return true;
            }
            if (keyData.Equals(Keys.Tab))
            {
                _controller.AddNote();

                return true;
            }
            else
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        /// <summary>
        /// Handles the Tick event of the positionTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void positionTimer_Tick(object sender, EventArgs e)
        {
            _controller.PositionUpdate();

        }

        /// <summary>
        /// Handles the Click event of the ResetStopperButton control.
        /// </summary> 
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ResetStopperButton_Click(object sender, EventArgs e)
        {
            _controller.ResetStopper();
        }

    }
}
