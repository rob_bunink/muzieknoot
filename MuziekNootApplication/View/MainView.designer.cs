﻿using System.Reflection;
using System.Windows.Forms;

namespace MuziekNootApplication.View
{
    partial class MainView 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LoadNotesButton = new System.Windows.Forms.Button();
            this.AddNotesButton = new System.Windows.Forms.Button();
            this.LoadSongButton = new System.Windows.Forms.Button();
            this.PlaySongButton = new System.Windows.Forms.Button();
            this.TimeLineContainer = new System.Windows.Forms.Panel();
            this.PauzeSongButton = new System.Windows.Forms.Button();
            this.tbPosition = new System.Windows.Forms.TrackBar();
            this.lblCurrentSong = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.tbVolume = new System.Windows.Forms.TrackBar();
            this.lblVolume = new System.Windows.Forms.Label();
            this.tbBalance = new System.Windows.Forms.TrackBar();
            this.lblBalance = new System.Windows.Forms.Label();
            this.cbMute = new System.Windows.Forms.CheckBox();
            this.cbLoop = new System.Windows.Forms.CheckBox();
            this.tbBass = new System.Windows.Forms.TrackBar();
            this.tbTreble = new System.Windows.Forms.TrackBar();
            this.lblBass = new System.Windows.Forms.Label();
            this.lbTreble = new System.Windows.Forms.Label();
            this.lblPositie = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbStartStopper = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbStopStopper = new System.Windows.Forms.Label();
            this.positionTimer = new System.Windows.Forms.Timer(this.components);
            this.ResetStopperButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tbPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LoadNotesButton
            // 
            this.LoadNotesButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.LoadNotesButton.Location = new System.Drawing.Point(659, 202);
            this.LoadNotesButton.Name = "LoadNotesButton";
            this.LoadNotesButton.Size = new System.Drawing.Size(97, 23);
            this.LoadNotesButton.TabIndex = 32;
            this.LoadNotesButton.Text = "Stop Song";
            this.LoadNotesButton.Click += new System.EventHandler(this.StopSong);
            // 
            // AddNotesButton
            // 
            this.AddNotesButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.AddNotesButton.Location = new System.Drawing.Point(659, 173);
            this.AddNotesButton.Name = "AddNotesButton";
            this.AddNotesButton.Size = new System.Drawing.Size(97, 23);
            this.AddNotesButton.TabIndex = 31;
            this.AddNotesButton.Text = "&Add Notes";
            this.AddNotesButton.Click += new System.EventHandler(this.AddNotes);
            // 
            // LoadSongButton
            // 
            this.LoadSongButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.LoadSongButton.Location = new System.Drawing.Point(659, 231);
            this.LoadSongButton.Name = "LoadSongButton";
            this.LoadSongButton.Size = new System.Drawing.Size(97, 23);
            this.LoadSongButton.TabIndex = 33;
            this.LoadSongButton.Text = "&Load Song";
            this.LoadSongButton.Click += new System.EventHandler(this.LoadSong);
            // 
            // PlaySongButton
            // 
            this.PlaySongButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PlaySongButton.Enabled = false;
            this.PlaySongButton.Location = new System.Drawing.Point(659, 260);
            this.PlaySongButton.Name = "PlaySongButton";
            this.PlaySongButton.Size = new System.Drawing.Size(97, 23);
            this.PlaySongButton.TabIndex = 35;
            this.PlaySongButton.Text = "Play Song";
            this.PlaySongButton.UseVisualStyleBackColor = true;
            this.PlaySongButton.Click += new System.EventHandler(this.PlaySong);
            // 
            // TimeLineContainer
            // 
            this.TimeLineContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeLineContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TimeLineContainer.BackColor = System.Drawing.Color.White;
            this.TimeLineContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TimeLineContainer.Cursor = System.Windows.Forms.Cursors.Default;
            this.TimeLineContainer.Location = new System.Drawing.Point(30, 436);
            this.TimeLineContainer.MaximumSize = new System.Drawing.Size(900000, 113);
            this.TimeLineContainer.MinimumSize = new System.Drawing.Size(726, 113);
            this.TimeLineContainer.Name = "TimeLineContainer";
            this.TimeLineContainer.Padding = new System.Windows.Forms.Padding(20);
            this.TimeLineContainer.Size = new System.Drawing.Size(726, 113);
            this.TimeLineContainer.TabIndex = 36;
            // 
            // PauzeSongButton
            // 
            this.PauzeSongButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PauzeSongButton.Enabled = false;
            this.PauzeSongButton.Location = new System.Drawing.Point(659, 289);
            this.PauzeSongButton.Name = "PauzeSongButton";
            this.PauzeSongButton.Size = new System.Drawing.Size(97, 23);
            this.PauzeSongButton.TabIndex = 37;
            this.PauzeSongButton.Text = "Pauze Song";
            this.PauzeSongButton.UseVisualStyleBackColor = true;
            this.PauzeSongButton.Click += new System.EventHandler(this.PauzeSong);
            // 
            // tbPosition
            // 
            this.tbPosition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPosition.Location = new System.Drawing.Point(56, 385);
            this.tbPosition.MaximumSize = new System.Drawing.Size(99999, 45);
            this.tbPosition.MinimumSize = new System.Drawing.Size(665, 45);
            this.tbPosition.Name = "tbPosition";
            this.tbPosition.Size = new System.Drawing.Size(665, 45);
            this.tbPosition.TabIndex = 38;
            this.tbPosition.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tbPosition_MouseUp);
            // 
            // lblCurrentSong
            // 
            this.lblCurrentSong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentSong.AutoSize = true;
            this.lblCurrentSong.Location = new System.Drawing.Point(53, 157);
            this.lblCurrentSong.Name = "lblCurrentSong";
            this.lblCurrentSong.Size = new System.Drawing.Size(72, 13);
            this.lblCurrentSong.TabIndex = 39;
            this.lblCurrentSong.Text = "[current song]";
            // 
            // lblLength
            // 
            this.lblLength.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(115, 178);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(42, 13);
            this.lblLength.TabIndex = 40;
            this.lblLength.Text = "[length]";
            // 
            // tbVolume
            // 
            this.tbVolume.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tbVolume.AutoSize = false;
            this.tbVolume.Location = new System.Drawing.Point(461, 160);
            this.tbVolume.Maximum = 1000;
            this.tbVolume.Name = "tbVolume";
            this.tbVolume.Size = new System.Drawing.Size(182, 45);
            this.tbVolume.TabIndex = 41;
            this.tbVolume.Value = 1000;
            this.tbVolume.ValueChanged += new System.EventHandler(this.tbVolume_ValueChanged);
            // 
            // lblVolume
            // 
            this.lblVolume.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblVolume.Location = new System.Drawing.Point(395, 160);
            this.lblVolume.Name = "lblVolume";
            this.lblVolume.Size = new System.Drawing.Size(45, 13);
            this.lblVolume.TabIndex = 42;
            this.lblVolume.Text = "Volume:";
            // 
            // tbBalance
            // 
            this.tbBalance.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tbBalance.AutoSize = false;
            this.tbBalance.Location = new System.Drawing.Point(461, 202);
            this.tbBalance.Maximum = 1000;
            this.tbBalance.Name = "tbBalance";
            this.tbBalance.Size = new System.Drawing.Size(182, 45);
            this.tbBalance.TabIndex = 43;
            this.tbBalance.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.tbBalance.Value = 500;
            this.tbBalance.ValueChanged += new System.EventHandler(this.tbBalance_ValueChanged);
            // 
            // lblBalance
            // 
            this.lblBalance.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblBalance.Location = new System.Drawing.Point(395, 202);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(49, 13);
            this.lblBalance.TabIndex = 44;
            this.lblBalance.Text = "Balance:";
            // 
            // cbMute
            // 
            this.cbMute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMute.AutoSize = true;
            this.cbMute.Location = new System.Drawing.Point(56, 230);
            this.cbMute.Name = "cbMute";
            this.cbMute.Size = new System.Drawing.Size(50, 17);
            this.cbMute.TabIndex = 45;
            this.cbMute.Text = "Mute";
            this.cbMute.UseVisualStyleBackColor = true;
            this.cbMute.CheckedChanged += new System.EventHandler(this.cbMute_CheckedChanged);
            // 
            // cbLoop
            // 
            this.cbLoop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbLoop.AutoSize = true;
            this.cbLoop.Location = new System.Drawing.Point(118, 230);
            this.cbLoop.Name = "cbLoop";
            this.cbLoop.Size = new System.Drawing.Size(50, 17);
            this.cbLoop.TabIndex = 46;
            this.cbLoop.Text = "Loop";
            this.cbLoop.UseVisualStyleBackColor = true;
            this.cbLoop.CheckedChanged += new System.EventHandler(this.cbLoop_CheckedChanged_1);
            // 
            // tbBass
            // 
            this.tbBass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBass.AutoSize = false;
            this.tbBass.Location = new System.Drawing.Point(399, 260);
            this.tbBass.Maximum = 1000;
            this.tbBass.Name = "tbBass";
            this.tbBass.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbBass.Size = new System.Drawing.Size(45, 104);
            this.tbBass.TabIndex = 47;
            this.tbBass.Value = 1000;
            this.tbBass.ValueChanged += new System.EventHandler(this.tbBass_ValueChanged);
            // 
            // tbTreble
            // 
            this.tbTreble.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTreble.AutoSize = false;
            this.tbTreble.Location = new System.Drawing.Point(461, 260);
            this.tbTreble.Maximum = 1000;
            this.tbTreble.Name = "tbTreble";
            this.tbTreble.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbTreble.Size = new System.Drawing.Size(45, 104);
            this.tbTreble.TabIndex = 48;
            this.tbTreble.Value = 1000;
            this.tbTreble.ValueChanged += new System.EventHandler(this.tbTreble_ValueChanged);
            // 
            // lblBass
            // 
            this.lblBass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBass.Location = new System.Drawing.Point(395, 241);
            this.lblBass.Name = "lblBass";
            this.lblBass.Size = new System.Drawing.Size(33, 13);
            this.lblBass.TabIndex = 49;
            this.lblBass.Text = "Bass:";
            // 
            // lbTreble
            // 
            this.lbTreble.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTreble.Location = new System.Drawing.Point(458, 241);
            this.lbTreble.Name = "lbTreble";
            this.lbTreble.Size = new System.Drawing.Size(40, 13);
            this.lbTreble.TabIndex = 50;
            this.lbTreble.Text = "Treble:";
            // 
            // lblPositie
            // 
            this.lblPositie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPositie.AutoSize = true;
            this.lblPositie.Location = new System.Drawing.Point(53, 178);
            this.lblPositie.Name = "lblPositie";
            this.lblPositie.Size = new System.Drawing.Size(43, 13);
            this.lblPositie.TabIndex = 51;
            this.lblPositie.Text = "[positie]";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "/";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MuziekNootApplication.Properties.Resources.logo_muzieknoot;
            this.pictureBox1.Location = new System.Drawing.Point(30, 4);
            this.pictureBox1.MaximumSize = new System.Drawing.Size(261, 150);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(261, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(297, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(353, 55);
            this.label2.TabIndex = 54;
            this.label2.Text = "De MuziekNoot";
            // 
            // lbStartStopper
            // 
            this.lbStartStopper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbStartStopper.AutoSize = true;
            this.lbStartStopper.Location = new System.Drawing.Point(79, 361);
            this.lbStartStopper.Name = "lbStartStopper";
            this.lbStartStopper.Size = new System.Drawing.Size(0, 13);
            this.lbStartStopper.TabIndex = 57;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 361);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "-";
            // 
            // lbStopStopper
            // 
            this.lbStopStopper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbStopStopper.AutoSize = true;
            this.lbStopStopper.Location = new System.Drawing.Point(176, 361);
            this.lbStopStopper.Name = "lbStopStopper";
            this.lbStopStopper.Size = new System.Drawing.Size(0, 13);
            this.lbStopStopper.TabIndex = 59;
            // 
            // positionTimer
            // 
            this.positionTimer.Tick += new System.EventHandler(this.positionTimer_Tick);
            // 
            // ResetStopperButton
            // 
            this.ResetStopperButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ResetStopperButton.Enabled = false;
            this.ResetStopperButton.Location = new System.Drawing.Point(239, 343);
            this.ResetStopperButton.Name = "ResetStopperButton";
            this.ResetStopperButton.Size = new System.Drawing.Size(97, 36);
            this.ResetStopperButton.TabIndex = 60;
            this.ResetStopperButton.Text = "Reset Start/stoppunt";
            this.ResetStopperButton.UseVisualStyleBackColor = true;
            this.ResetStopperButton.Click += new System.EventHandler(this.ResetStopperButton_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.ResetStopperButton);
            this.Controls.Add(this.lbStopStopper);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbStartStopper);
            this.Controls.Add(this.tbPosition);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPositie);
            this.Controls.Add(this.lbTreble);
            this.Controls.Add(this.lblBass);
            this.Controls.Add(this.tbTreble);
            this.Controls.Add(this.tbBass);
            this.Controls.Add(this.cbLoop);
            this.Controls.Add(this.cbMute);
            this.Controls.Add(this.lblBalance);
            this.Controls.Add(this.tbBalance);
            this.Controls.Add(this.lblVolume);
            this.Controls.Add(this.tbVolume);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.lblCurrentSong);
            this.Controls.Add(this.PauzeSongButton);
            this.Controls.Add(this.TimeLineContainer);
            this.Controls.Add(this.PlaySongButton);
            this.Controls.Add(this.LoadNotesButton);
            this.Controls.Add(this.AddNotesButton);
            this.Controls.Add(this.LoadSongButton);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.HelpButton = true;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainView";
            this.Text = "MuziekNoot";
            ((System.ComponentModel.ISupportInitialize)(this.tbPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTreble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Button LoadNotesButton;
        internal System.Windows.Forms.Button AddNotesButton;
        private System.Windows.Forms.Button PlaySongButton;
        private System.Windows.Forms.Panel TimeLineContainer;
        internal System.Windows.Forms.Button LoadSongButton;
        private System.Windows.Forms.Button PauzeSongButton;
        private System.Windows.Forms.TrackBar tbPosition;
        public System.Windows.Forms.Label lblCurrentSong;
        public System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.TrackBar tbVolume;
        public System.Windows.Forms.Label lblVolume;
        private System.Windows.Forms.TrackBar tbBalance;
        public System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.CheckBox cbMute;
        private System.Windows.Forms.CheckBox cbLoop;
        private System.Windows.Forms.TrackBar tbBass;
        private System.Windows.Forms.TrackBar tbTreble;
        public System.Windows.Forms.Label lblBass;
        public System.Windows.Forms.Label lbTreble;
        public System.Windows.Forms.Label lblPositie;
        public System.Windows.Forms.Label label1;
        private PictureBox pictureBox1;
        private Label label2;
        private Label lbStartStopper;
        private Label label4;
        private Label lbStopStopper;
        private Timer positionTimer;
        private Button ResetStopperButton;
    }
}

