﻿namespace MuziekNootApplication.View
{
   public interface IObserver
    {
        void RecieveUpdate(int TickCount); 
    }
}
