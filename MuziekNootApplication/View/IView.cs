﻿//Copyright (C) MuziekNoot. All right reserved.
//IView.cs
//compile with: /doc: IView.xml

using System.Windows.Forms;
using MuziekNootApplication.Controller;

namespace MuziekNootApplication.View
{
    public interface IView
    {
        Timer getPositionTimer(); 

        void SetController(MainViewController controller);

        ITimeline GetTimeline();

        void MoveTimeLine(int px);

        Label GetCurrentSongLabel();
        Label GetLengthLabel();
        Label GetPositionLabel();
        void RestoreTimeline();
        TrackBar GetTbPostion();
        Label GetlbStartStopper();
        Label GetlbStopStopper();
        Button GetResetStopperButton();
    }
}
