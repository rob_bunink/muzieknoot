﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using MuziekNootApplication.Model;
using TagLib.Riff;

namespace MuziekNootApplication.View.AddNotes
{

    public partial class AddGuitarChords : UserControl 
    {
        /// <summary>
        /// The last selected note index
        /// </summary>
        private int LastSelected= -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddGuitarChords"/> class.
        /// </summary>
        public AddGuitarChords()
        {
            InitializeComponent();

            Draw();
        }

        /// <summary>
        /// Draws the notes on the screen.
        /// </summary>
        private void Draw()
        {
            listBox1.Items.Clear();
            if (GuitarChordsTimeLine.Notes != null)
                foreach (var note in GuitarChordsTimeLine.Notes)
                {
                    listBox1.Items.Add(note.DisplayName + " - " + note.Timestamp);
                }
        }

        /// <summary>
        /// Handles the Click event of the Add control.
        /// adds a note with the filled in values
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Add_Click(object sender, EventArgs e)
        {
            try
            {
                var note = new GuitarChord(this.NameBox.Text, Convert.ToDecimal(this.TimeStamp.Text));
                GuitarChordsTimeLine.Notes.Add(note);
            }
            catch (Exception z)
            {
                // invalid note
            }
            Draw();
        }

        /// <summary>
        /// Handles the Click event of the Remove control.
        /// deletes the selected note
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Remove_Click(object sender, EventArgs e)
        {
            if ( listBox1.SelectedIndex>-1)
                GuitarChordsTimeLine.Notes.RemoveAt(listBox1.SelectedIndex);
            Draw();
        }

        /// <summary>
        /// Handles the Click event of the Ok control.
        /// disposes this control
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Ok_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        /// <summary>
        /// Saves the notes to a file in "MyDocuments".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Save_Click(object sender, EventArgs e)
        {
            var a = new JavaScriptSerializer().Serialize(GuitarChordsTimeLine.Notes);
            var b = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            GuidString = GuidString.Replace("\\", "");
            GuidString = GuidString.Replace("/", "");
            System.IO.File.WriteAllText(b + "\\" + GuidString + ".txt",a);

        }

        /// <summary>
        /// Handles the Click event of the Load control.
        /// opens a file select to load a note file
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Load_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = true;
            var json = "";
            // Call the ShowDialog method to show the dialog box.
            var userClickedOK = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == DialogResult.OK )
            {
                // Open the selected file to read.
                System.IO.Stream fileStream = openFileDialog1.OpenFile();

                using (System.IO.StreamReader reader = new System.IO.StreamReader(fileStream))
                {
                    // Read the first line from the file and write it the textbox.
                    json = reader.ReadLine();
                }
                fileStream.Close();
            }
            try
            {
                GuitarChordsTimeLine.Notes = new JavaScriptSerializer().Deserialize<List<GuitarChord>>(json);
            }
            catch (Exception z)
            {
                //Invalid file
            }
            Draw();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the listBox1 control.
        /// Fills the name and timestamp boxes with the selected note
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBox1.SelectedIndex>-1)
            {
                var note = GuitarChordsTimeLine.Notes[this.listBox1.SelectedIndex];
                this.NameBox.Text = note.DisplayName;
                this.TimeStamp.Text = note.Timestamp.ToString();
                LastSelected = this.listBox1.SelectedIndex;
            }
            else {
                this.NameBox.Text = "";
                this.TimeStamp.Text = "";
            }
        }

        /// <summary>
        /// Handles the MouseClick event of the listBox1 control.
        /// Looks if the click is outside of a note in the list. if it is it will deselect any selected notes
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Y > listBox1.ItemHeight*listBox1.Items.Count)
            {
                listBox1.SelectedItems.Clear();
                LastSelected = -1;
            }
        }


        private void NameBox_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        /// <summary>
        /// Handles the KeyUp event of the NameBox control.
        /// changes a current note
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void NameBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (LastSelected != -1)
            {
                GuitarChordsTimeLine.Notes[LastSelected] = new GuitarChord(this.NameBox.Text, Convert.ToDecimal(this.TimeStamp.Text));
            }
            Draw();
        }

        /// <summary>
        /// Handles the MouseUp event of the listBox1 control.
        /// clears all selected on right click
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void listBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                listBox1.SelectedItems.Clear();
                LastSelected = -1;
            }
        }
    }

}