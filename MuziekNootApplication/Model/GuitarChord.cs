﻿//Copyright (C) MuziekNoot. All right reserved.
//GuitarChord.cs
//compile with: /doc: GuitarChord.xml

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuziekNootApplication.Model
{
    /// <summary>
    /// Class for a guitarchord
    /// </summary>
    /// <seealso cref="MuziekNootApplication.Model.INote" />
    public class GuitarChord : INote
    {
        /// <summary>
        /// The display name
        /// </summary>
        public string DisplayName; 

        /// <summary>
        /// The timestamp
        /// </summary>
        public decimal Timestamp;

        /// <summary>
        /// Initializes a new instance of the <see cref="GuitarChord"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="timestamp">The timestamp.</param>
        public GuitarChord(string name, decimal timestamp)
        {
            DisplayName = name;
            Timestamp = timestamp;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GuitarChord"/> class.
        /// </summary>
        public GuitarChord()
        {
                
        }
    }

}