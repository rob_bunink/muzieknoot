﻿//Copyright (C) MuziekNoot. All right reserved.
//IAdvancedMediaPlayer.cs
//compile with: /doc: IAdvancedMediaPlayer.xml

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuziekNootApplication.Model.MediaPlayer
{
    interface IAdvancedMediaPlayer
    {
        void OpenFile(string filename);
        void Play();
        void Pauze();
        void Stop();
        void Close();
        ulong GetLength();
        ulong GetCurrentPosition();
        bool IsPlaying();
        string GetAlbumNaam();
        string GetTitel();
        string GetArtist();
        void SetMuteAll(bool @checked);
        void SetLoop(bool @checked);
        void SetVolume(int @value);
        void SetBalance(int value);
        void SetBass(int value);
        void SetTreble(int value);
        string GetInfo();
        void SetPosition(ulong value);
        bool isOpened();
    }
}
