﻿//Copyright (C) MuziekNoot. All right reserved.
//Mp3Player.cs
//compile with: /doc: Mp3Player.xml

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MuziekNootApplication.Model.MediaPlayer
{
    class Mp3Player : IAdvancedMediaPlayer
    {
        private ulong Lng;
        private bool Opened { get; set; }
        private bool Playing { get; set; }
        private bool Paused { get; set; }
        private bool MuteAll { get; set; }
        private int aVolume { get; set; }
        private int tVolume { get; set; }
        private int VolBalance { get; set; }
        private bool Loop { get; set; }

        TagLib.File SongInfo { get; set; }
        [DllImport("winmm.dll")]
        private static extern long mciSendString(string lpstrCommand, StringBuilder lpstrReturnString, int uReturnLength, IntPtr hwndCallback);

        /// <summary>
        /// Constructor
        /// </summary>
        public Mp3Player()
        {

        }

        /// <summary>
        /// Start het nummer
        /// </summary>
        public void Play()
        {
            if (!Playing)
            {
                string command = "play MediaFile";
                if (Loop) command += " REPEAT";
                mciSendString(command, null, 0, IntPtr.Zero);
                Playing = true;
            }
        }

        /// <summary>
        /// Pauze het nummer
        /// </summary>
        public void Pauze()
        {
            string command = "stop MediaFile";
            mciSendString(command, null, 0, IntPtr.Zero);
            Playing = false;
            Paused = true;
        }

        /// <summary>
        /// Stop het nummer
        /// </summary>
        public void Stop()
        {
            string command = "stop MediaFile";
            mciSendString(command, null, 0, IntPtr.Zero);
            Playing = false;
        }

        /// <summary>
        /// Sluit het nummer
        /// </summary>
        public void Close()
        {
            if (Playing)
            {
                Stop();
            }
            if (!Opened) return;
            string command = "close MediaFile";
            mciSendString(command, null, 0, IntPtr.Zero);
            Opened = false;
            Playing = false;
            Paused = false;
        }

        /// <summary>
        /// Lengte van liedje in ms
        /// </summary>
        /// <returns></returns>
        public ulong GetLength()
        {
            return Lng;
        }




        /// <summary>
        /// De lengte van liedje berekenen
        /// </summary>
        private void CalculateLength()
        {
            try
            {
                StringBuilder str = new StringBuilder(128);
                string command = "status MediaFile length";
                mciSendString(command, str, 128, IntPtr.Zero);
                Lng = Convert.ToUInt64(str.ToString());
            }
            catch
            {
                // ignored
            }
        }


        /// <summary>
        /// Albumnaam van liedje in
        /// </summary>
        /// <returns></returns>
        public string GetAlbumNaam()
        {
            return Opened ? SongInfo.Tag.Album : "";
        }

        /// <summary>
        /// Titel van liedje in
        /// </summary>
        /// <returns></returns>
        public string GetTitel()
        {
            return Opened ? SongInfo.Tag.Title : "";
        }

        /// <summary>
        /// Artist(en) van liedje in
        /// </summary>
        /// <returns></returns>
        public string GetArtist()
        {
            return Opened ? string.Join(" / ", SongInfo.Tag.Performers) : "";
        }

        /// <summary>
        /// Mute de audio of zet weer geluid aan.
        /// </summary>
        /// <param name="checked"></param>
        public void SetMuteAll(bool @checked)
        {
            MuteAll = @checked;
            StringBuilder str = new StringBuilder(128);
            string command; 
            if (MuteAll)
            {
                command = "setaudio MediaFile off";
                mciSendString(command, str, 128, IntPtr.Zero);
            }
            else
            {
                command = "setaudio MediaFile on";
                mciSendString(command, str, 128, IntPtr.Zero);
            }
        }

        /// <summary>
        /// Zet de player om de blijven lopen over hetzelfde nummer.
        /// </summary>
        /// <param name="checked"></param>
        public void SetLoop(bool @checked)
        {
            Loop = @checked;
        }


        /// <summary>
        /// Stel volume in.
        /// </summary>
        /// <param name="value"></param>
        public void SetVolume(int @value)
        {
            if (!Opened || (value < 0 || value > 1000)) return;
            aVolume = value;
            string command = "setaudio MediaFile volume to "+ aVolume + "";
            mciSendString(command, null, 0, IntPtr.Zero);
        }

        /// <summary>
        /// Stel de balance anders in.
        /// </summary>
        /// <param name="value"></param>
        public void SetBalance(int value)
        {
            if (Opened && (value >= -1000 && value <= 1000))
            {
                VolBalance = value;
                double vPct = Convert.ToDouble(aVolume) / 1000.0;

                string command;
                if (value < 0)
                {
                    command = string.Format("setaudio MediaFile left volume to {0:#}", aVolume);
                    mciSendString(command, null, 0, IntPtr.Zero);
                    command = string.Format("setaudio MediaFile right volume to {0:#}", (1000 + value) * vPct);
                    mciSendString(command, null, 0, IntPtr.Zero);
                }
                else
                {
                    command = string.Format("setaudio MediaFile right volume to {0:#}", aVolume);
                    mciSendString(command, null, 0, IntPtr.Zero);
                    command = string.Format("setaudio MediaFile left volume to {0:#}", (1000 - value) * vPct);
                    mciSendString(command, null, 0, IntPtr.Zero);
                }
            }
        }


        /// <summary>
        /// Stel bass in.
        /// </summary>
        /// <param name="value"></param>
        public void SetBass(int value)
        {
            if (!Opened || (value < 0 || value > 1000)) return;
            tVolume = value;
            string command = "setaudio MediaFile bass to " + tVolume + "";
            mciSendString(command, null, 0, IntPtr.Zero);
        }

        /// <summary>
        /// Stel treble in.
        /// </summary>
        /// <param name="value"></param>
        public void SetTreble(int value)
        {
            if (!Opened || (value < 0 || value > 1000)) return;
            tVolume = value;
            string command = "setaudio MediaFile treble to " + tVolume + "";
            mciSendString(command, null, 0, IntPtr.Zero);
        }

        public string GetInfo()
        {
            if (GetArtist() == "")
            {
                var fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(SongInfo.Name);
                if (fileNameWithoutExtension != null)
                    return fileNameWithoutExtension.ToString();
            }

            return GetArtist() + " - " + GetTitel() + "(" +
                              GetAlbumNaam() + ")";
        }

        /// <summary>
        /// Stel een nieuwe positie in.
        /// </summary>
        /// <param name="value"></param>
        public void SetPosition(ulong value)
        {
            string command;
            if (Opened && value <= Lng)
            {
                command = "seek MediaFile to " + value + "";
                mciSendString(command, null, 0, IntPtr.Zero);
                if (Playing)
                {
                    Playing = false;
                    Play();
                }
            }
        }

        /// <summary>
        /// Geeft de status terug of een bestand geopend is.
        /// </summary>
        /// <returns></returns>
        public bool isOpened()
        {
            return Opened;
        }


        /// <summary>
        /// Huidige positie in ms
        /// </summary>
        /// <returns></returns>
        public ulong GetCurrentPosition()
        {
            ulong position;
            if (Opened && Playing)
            {
                StringBuilder returnData;
                int error;
                string command;
                command = "status MediaFile position";

                returnData = new StringBuilder(128);
                error = (int) mciSendString(command, returnData, returnData.Capacity, IntPtr.Zero);
                return ulong.Parse(returnData.ToString());
            }
            else
                return 0;

        }


        /// <summary>
        /// Geef terug of liedje aan spelen is.
        /// </summary>
        /// <returns></returns>
        public bool IsPlaying()
        {
            return Playing;
        }


        /// <summary>
        /// Start een file
        /// </summary>
        /// <param name="filename"></param>
        public void OpenFile(string filename)
        {
            string FORMAT = @"open ""{0}"" type mpegvideo alias MediaFile";
            string command = String.Format(FORMAT, filename);
            mciSendString(command, null, 0, IntPtr.Zero);
            Opened = true;
            SongInfo = TagLib.File.Create(filename);

            FORMAT = @"set ""{0}"" time format milliseconds";
            mciSendString(command, null, 0, IntPtr.Zero);
            FORMAT = @"set ""{0}"" seek exactly on";
            mciSendString(command, null, 0, IntPtr.Zero);
            CalculateLength();
        }
    }
}
