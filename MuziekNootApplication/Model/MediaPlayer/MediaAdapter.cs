﻿//Copyright (C) MuziekNoot. All right reserved.
//MediaAdapter.cs
//compile with: /doc: MediaAdapter.xml

using System;

namespace MuziekNootApplication.Model.MediaPlayer
{

    #region Adapter Pattern
    /// <summary>
    /// MediaAdapter voor het switchen van verschillende formaten.
    /// </summary>
    internal class MediaAdapter : IMediaPlayer
    {
        private readonly IAdvancedMediaPlayer _advancedMusicPlayer;

        /// <summary>
        /// Open een bestand.
        /// </summary>
        /// <param name="audioType"></param>
        /// <param name="fileName"></param>
        public void OpenFile(string audioType, string fileName)
        {
            if (audioType.Equals("mp3"))
            {
                _advancedMusicPlayer.OpenFile(fileName);
            }
        }

        /// <summary>
        /// Play het nummer via de media speler
        /// </summary>
        public void Play()
        {
            if (_advancedMusicPlayer.GetLength() > 0)
            {
                _advancedMusicPlayer.Play();
            }
        }

        /// <summary>
        /// Pauze de media speler
        /// </summary>
        public void Pauze()
        {
            _advancedMusicPlayer.Pauze();
        }

        /// <summary>
        /// Stop de media speler
        /// </summary>
        public void Stop()
        {
            _advancedMusicPlayer.Stop();
        }

        /// <summary>
        /// Sluit de media speler
        /// </summary>
        public void Close()
        {
            _advancedMusicPlayer.Close();
        }

        /// <summary>
        /// Geef de status of een media speler aan spelen is.
        /// </summary>
        public bool IsPlaying()
        {
            return _advancedMusicPlayer.IsPlaying();
        }

        /// <summary>
        /// Geef de lengte van een liedje uit een media speler.
        /// </summary>
        public ulong GetLength()
        {
            return _advancedMusicPlayer.GetLength();
        }

        /// <summary>
        /// Geef de lengte van een liedje uit een media speler.
        /// </summary>
        public ulong GetCurrentPosition()
        {
            return _advancedMusicPlayer.GetCurrentPosition();
        }

        /// <summary>
        /// Info van de media in de speler
        /// </summary>
        /// <returns></returns>
        public string GetInfo()
        {
            return _advancedMusicPlayer.GetInfo();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="audioType"></param>
        public MediaAdapter(String audioType)
        {
            if (audioType.Equals("mp3"))
            {
                _advancedMusicPlayer = new Mp3Player();

            }
            else if (audioType.Equals("flac"))
            {
                _advancedMusicPlayer = new FlacPlayer();
            }
        }


        /// <summary>
        /// Zet het geluid uit.
        /// </summary>
        /// <param name="checked"></param>
        public void SetMuteAll(bool @checked)
        {
            _advancedMusicPlayer.SetMuteAll(@checked);
        }


        /// <summary>
        /// Stel de speler in om te blijven lopen.
        /// </summary>
        /// <param name="checked"></param>
        public void SetLoop(bool @checked)
        {
            _advancedMusicPlayer.SetLoop(@checked);
        }

        /// <summary>
        /// Zet het volume op een andere waarde.
        /// </summary>
        /// <param name="value"></param>
        public void SetVolume(int value)
        {
            _advancedMusicPlayer.SetVolume(value);
        }

        /// <summary>
        /// Geef de balance een andere waarde
        /// </summary>
        /// <param name="value"></param>
        public void SetBalance(int value)
        {
            _advancedMusicPlayer.SetBalance(value);
        }

        /// <summary>
        /// Geef de bass een andere waarde
        /// </summary>
        /// <param name="value"></param>
        public void SetBass(int value)
        {
            _advancedMusicPlayer.SetBass(value);
        }

        /// <summary>
        /// Geef de treble een andere waarde
        /// </summary>
        /// <param name="value"></param>
        public void SetTreble(int value)
        {
            _advancedMusicPlayer.SetTreble(value);
        }

        /// <summary>
        /// De positie van nummer krijgt een andere waarde
        /// </summary>
        /// <param name="value"></param>
        public void SetPosition(ulong value)
        {
            _advancedMusicPlayer.SetPosition(value);
        }

        /// <summary>
        /// Controleer of er een bestand is geopend.
        /// </summary>
        public bool IsOpened()
        {
            return _advancedMusicPlayer.isOpened();
        }
    }

    #endregion
}
