﻿//Copyright (C) MuziekNoot. All right reserved.
//IMediaPlayer.cs
//compile with: /doc: IMediaPlayer.xml

using System;

namespace MuziekNootApplication.Model.MediaPlayer
{
    #region Adapter Pattern
    /// <summary>
    /// IMediaPlayer
    /// </summary>
    interface IMediaPlayer
    {
        /// <summary>
        /// Open file
        /// </summary>
        /// <param name="audioType"></param>
        /// <param name="fileName"></param>
        void OpenFile(String audioType, String fileName);
        /// <summary>
        /// Play Media
        /// </summary>
        void Play();
        /// <summary>
        /// Pauze Media
        /// </summary>
        void Pauze();
        /// <summary>
        /// Stop Media
        /// </summary>
        void Stop();
        /// <summary>
        /// Geef de status of er media gespeelt wordt.
        /// </summary>
        /// <returns></returns>
        bool IsPlaying();
        /// <summary>
        /// Huidige positie in ms
        /// </summary>
        /// <returns></returns>
        ulong GetCurrentPosition();
        /// <summary>
        /// De lengte van media in ms
        /// </summary>
        /// <returns></returns>
        ulong GetLength();
        /// <summary>
        /// Zet de mute functie aan.
        /// </summary>
        /// <param name="checked"></param>
        void SetMuteAll(bool @checked);
        /// <summary>
        /// Zet de loop functie aan.
        /// </summary>
        /// <param name="checked"></param>
        void SetLoop(bool @checked);
        /// <summary>
        /// Stel volume in.
        /// </summary>
        /// <param name="value"></param>
        void SetVolume(int @value);
        /// <summary>
        /// Zet de belance op een andere waarde
        /// </summary>
        /// <param name="value"></param>
        void SetBalance(int value);
        /// <summary>
        /// Zet de bass op een andere waarde
        /// </summary>
        /// <param name="value"></param>
        void SetBass(int value);
        /// <summary>
        /// Zet de Treble op een andere waarde
        /// </summary>
        /// <param name="value"></param>
        void SetTreble(int value);

    }
    #endregion
}
