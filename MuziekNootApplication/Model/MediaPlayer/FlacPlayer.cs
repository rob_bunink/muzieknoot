﻿//Copyright (C) MuziekNoot. All right reserved.
//FlacPlayer.cs
//compile with: /doc: FlacPlayer.xml

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuziekNootApplication.Model.MediaPlayer
{
    class FlacPlayer : IAdvancedMediaPlayer
    {
        public void OpenFile(string filename)
        {
            throw new NotImplementedException();
        }

        public void Play()
        {
            throw new NotImplementedException();
        }

        public void Pauze()
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public void Close()
        {
            throw new NotImplementedException();
        }

        public ulong GetLength()
        {
            throw new NotImplementedException();
        }

        public ulong GetCurrentPosition()
        {
            throw new NotImplementedException();
        }

        public bool IsPlaying()
        {
            throw new NotImplementedException();
        }

        public string GetAlbumNaam()
        {
            throw new NotImplementedException();
        }

        public string GetTitel()
        {
            throw new NotImplementedException();
        }

        public string GetArtist()
        {
            throw new NotImplementedException();
        }

        public void SetMuteAll(bool @checked)
        {
            throw new NotImplementedException();
        }

        public void SetLoop(bool @checked)
        {
            throw new NotImplementedException();
        }

        public void SetVolume(int value)
        {
            throw new NotImplementedException();
        }

        public void SetBalance(int value)
        {
            throw new NotImplementedException();
        }

        public void SetBass(int value)
        {
            throw new NotImplementedException();
        }

        public void SetTreble(int value)
        {
            throw new NotImplementedException();
        }

        public string GetInfo()
        {
            throw new NotImplementedException();
        }

        public void SetPosition(ulong value)
        {
            throw new NotImplementedException();
        }

        public bool isOpened()
        {
            throw new NotImplementedException();
        }
    }
}
