﻿//Copyright (C) MuziekNoot. All right reserved.
//AudioPlayer.cs
//compile with: /doc: AudioPlayer.xml

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuziekNootApplication.Model.MediaPlayer
{
    /// <summary>
    /// AudioPlayer
    /// </summary>
    class AudioPlayer : IMediaPlayer
    {
        /// <summary>
        /// MediaAdapter Property
        /// </summary>
        private MediaAdapter _mediaAdapter;

        private string audiotype;
        private string filename;
        /// <summary>
        /// Open een liedje
        /// </summary>
        /// <param name="audioType"></param>
        /// <param name="fileName"></param>
        public void OpenFile(string audioType, string fileName)
        {
            //Controleer of de media adapter bestaat, anders sluit het huidige nummer.
            _mediaAdapter?.Close();
            if (audioType.Equals("mp3"))
            {
                _mediaAdapter = new MediaAdapter(audioType);
                _mediaAdapter.OpenFile(audioType, fileName);
                audiotype = audioType;
                filename = fileName;
            }

            else
            {
                Console.WriteLine("Invalid media. " + audioType + " format not supported");
            }
        }

        /// <summary>
        /// Play de media
        /// </summary>
        public void Play()
        {
            _mediaAdapter?.Play();
        }

        /// <summary>
        /// Pauze de media
        /// </summary>
        public void Pauze()
        {
            _mediaAdapter?.Pauze();
        }

        /// <summary>
        /// Stop de media
        /// </summary>
        public void Stop()
        {
            _mediaAdapter?.Close();
           
                _mediaAdapter = new MediaAdapter(audiotype);
                _mediaAdapter.OpenFile(audiotype, filename);
           
        }

        /// <summary>
        /// Geeft de status terug of liedje speelt.
        /// </summary>
        /// <returns></returns>
        public bool IsPlaying()
        {
            return _mediaAdapter != null && _mediaAdapter.IsPlaying();
        }

        /// <summary>
        /// Huidige positie
        /// </summary>
        /// <returns></returns>
        public ulong GetCurrentPosition()
        {
            if (IsPlaying()) { 
                return _mediaAdapter.GetCurrentPosition();
            }
            return 0;
        }

        /// <summary>
        /// De lengte van de media
        /// </summary>
        /// <returns></returns>
        public ulong GetLength()
        {
            return _mediaAdapter.GetLength();
        }

        /// <summary>
        /// Haal de info op van een nummer.
        /// </summary>
        /// <returns></returns>
        public string GetInfo()
        {
            return _mediaAdapter.GetInfo();
        }

        /// <summary>
        /// Zet het geluid uit in de player.
        /// </summary>
        /// <param name="checked"></param>
        public void SetMuteAll(bool @checked)
        {
            _mediaAdapter.SetMuteAll(@checked);
        }

        /// <summary>
        /// Zet het liedje op lopen.
        /// </summary>
        /// <param name="checked"></param>
        public void SetLoop(bool @checked)
        {
            _mediaAdapter.SetLoop(@checked);
        }

        /// <summary>
        /// Zet het geluid harder of zachter.
        /// </summary>
        /// <param name="value"></param>
        public void SetVolume(int @value)
        {
            _mediaAdapter.SetVolume(@value);
        }

        /// <summary>
        /// Stel een andere belance in
        /// </summary>
        /// <param name="value"></param>
        public void SetBalance(int value)
        {
            _mediaAdapter.SetBalance(@value);
        }

        /// <summary>
        /// Stel een andere bass waarde in
        /// </summary>
        /// <param name="value"></param>
        public void SetBass(int value)
        {
            _mediaAdapter.SetBass(@value);
        }

        /// <summary>
        /// Stel een andere treble waarde in
        /// </summary>
        /// <param name="value"></param>
        public void SetTreble(int value)
        {
            _mediaAdapter.SetTreble(@value);
        }

        /// <summary>
        /// De positie van nummer krijgt een andere waarde
        /// </summary>
        /// <param name="value"></param>
        public void SetPosition(ulong value)
        {
            if (_mediaAdapter == null) return;
            if( _mediaAdapter.IsOpened())
            {
                _mediaAdapter.SetPosition(@value);
            }
        }
    }
}
