﻿//Copyright (C) MuziekNoot. All right reserved.
//Program.cs
//compile with: /doc: Program.xml

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MuziekNootApplication.Controller;
using MuziekNootApplication.View;

namespace MuziekNootApplication
{
    class Program
    {
        #region SingleTons
        private static MainView view;
        private static MainViewController mainViewController;
        #endregion
        [STAThread]
        static void Main(string[] args)
        {
            view = MainView.Instance;
            view.Visible = false;

            // Add some dummy data

            MainViewController._view = view;
            mainViewController = MainViewController.Instance;
            view.SetController(mainViewController);

            view.ShowDialog();
        }
    }
}
