﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MuziekNootApplication.Model;
using MuziekNootApplication.View;

namespace MuziekNootApplicationTests.Controller
{
    [TestClass()]
    public class MainViewControllerTests
    {

        /// <summary>
        /// Test the adding of notes
        /// </summary>
        [TestMethod()] 
        public void AddNoteTest()
        {
            var a = new GuitarChordsTimeLine();
            var b = new GuitarChord();
            b.Timestamp = 1;
            b.DisplayName = "Em";
            a.AddNote(b);
            Assert.AreEqual(GuitarChordsTimeLine.Notes.Count,1);
        }
        
    }
}