﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WinFormMVC.Controller;

namespace WinFormMVC.View
{
    public partial class MainView : Form, IView
    {
        public MainView()
        {
            InitializeComponent();
        }

        MainViewController _controller;

        #region Events raised  back to controller

        private void AddNotes(object sender, EventArgs e)
        {
            this._controller.AddSong();
        }

        private void LoadNotes(object sender, EventArgs e)
        {
            this._controller.LoadNotes();
        }

        private void PlaySong(object sender, EventArgs e)
        {
            this._controller.PlaySong();
        }

        private void LoadSong(object sender, EventArgs e)
        {
            this._controller.Save();
        }

       
        #endregion

        #region ICatalogView implementation

        public void SetController(MainViewController controller)
        {
            _controller = controller;
        }



        #endregion

       
    }
}
