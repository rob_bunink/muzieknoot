﻿namespace WinFormMVC.View
{
    partial class MainView 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoadNotesButton = new System.Windows.Forms.Button();
            this.AddNotesButton = new System.Windows.Forms.Button();
            this.LoadSongButton = new System.Windows.Forms.Button();
            this.PlaySongButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LoadNotesButton
            // 
            this.LoadNotesButton.Location = new System.Drawing.Point(641, 54);
            this.LoadNotesButton.Name = "LoadNotesButton";
            this.LoadNotesButton.Size = new System.Drawing.Size(97, 23);
            this.LoadNotesButton.TabIndex = 32;
            this.LoadNotesButton.Text = "&Load Notes";
            this.LoadNotesButton.Click += new System.EventHandler(this.LoadNotes);
            // 
            // AddNotesButton
            // 
            this.AddNotesButton.Location = new System.Drawing.Point(641, 25);
            this.AddNotesButton.Name = "AddNotesButton";
            this.AddNotesButton.Size = new System.Drawing.Size(97, 23);
            this.AddNotesButton.TabIndex = 31;
            this.AddNotesButton.Text = "&Add Notes";
            this.AddNotesButton.Click += new System.EventHandler(this.AddNotes);
            // 
            // LoadSongButton
            // 
            this.LoadSongButton.Location = new System.Drawing.Point(641, 83);
            this.LoadSongButton.Name = "LoadSongButton";
            this.LoadSongButton.Size = new System.Drawing.Size(97, 23);
            this.LoadSongButton.TabIndex = 33;
            this.LoadSongButton.Text = "&Load Song";
            this.LoadSongButton.Click += new System.EventHandler(this.LoadSong);
            // 
            // PlaySongButton
            // 
            this.PlaySongButton.Location = new System.Drawing.Point(641, 112);
            this.PlaySongButton.Name = "PlaySongButton";
            this.PlaySongButton.Size = new System.Drawing.Size(97, 23);
            this.PlaySongButton.TabIndex = 35;
            this.PlaySongButton.Text = "Play Song";
            this.PlaySongButton.UseVisualStyleBackColor = true;
            this.PlaySongButton.Click += new System.EventHandler(this.PlaySong);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 459);
            this.Controls.Add(this.PlaySongButton);
            this.Controls.Add(this.LoadNotesButton);
            this.Controls.Add(this.AddNotesButton);
            this.Controls.Add(this.LoadSongButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainView";
            this.Text = "User List - Active Users";
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.Button LoadNotesButton;
        internal System.Windows.Forms.Button AddNotesButton;
        internal System.Windows.Forms.Button LoadSongButton;
        private System.Windows.Forms.Button PlaySongButton;
    }
}

