﻿using System;
using System.Collections;

namespace WinFormMVC.Controller
{
    public class MainViewController
    {
        IView _view;

        public MainViewController(IView view)
        {
            _view = view;
            view.SetController(this);
        }
        
        public void LoadView()
        {
         
        }

        public void AddSong()
        {
           
        }


        public void Save()
        {
         
        }

        public void PlaySong()
        {
           
        }

        public void LoadNotes()
        {
          
        }
    }
}
